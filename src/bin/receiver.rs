use std::net::ToSocketAddrs;
use tunnel::TunnelError;

const DEFAULT_BIND_ADDR: &str = "localhost:10443";

fn main() {
    let mut args = std::env::args();

    args.next(); // Throw away the first arg, it's just the name of the binary

    let psk = if let Some(a) = args.next() {
        a
    } else {
        eprintln!("You should provide the pre-shared key as the first argument!");
        std::process::exit(2);
    };

    let addr = args.next().unwrap_or_else(|| DEFAULT_BIND_ADDR.to_owned());

    let sock_addr = match addr.to_socket_addrs().map_err(|e| TunnelError::from(e, "")).and_then(|mut a| a.next().ok_or(TunnelError::new("No parseable address."))) {
        Ok(a) => a,
        Err(e) => {
            eprintln!("Couldn't parse the socket address \"{}\"! {}", addr, e);
            std::process::exit(3);
        }
    };

    let listener = match std::net::TcpListener::bind(sock_addr) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Couldn't bind the address! {}", e);
            std::process::exit(4);
        }
    };

    let stdout = std::io::stdout();
    let mut handle = stdout.lock();

    eprintln!("Listening on {}", addr);
    for stream_res in listener.incoming() {
        let stream = match stream_res {
            Ok(s) => s,
            Err(e) => {
                eprintln!("Error while establishing an incoming connection! {}", e);
                continue;
            }
        };
        let mut stream =
            match tunnel::server(stream, tunnel::SimplePskProvider::new(psk.as_bytes())) {
                Ok(s) => s,
                Err(e) => {
                    eprintln!("Error while initialising the connection! {}", e);
                    continue;
                }
            };
        
        eprintln!("Start incoming connection.");

        match std::io::copy(&mut stream, &mut handle) {
            Ok(bytes) => {
                eprintln!("{} bytes received.", bytes);
            }
            Err(e) => {
                eprintln!("Error while receiving! {}", e);
                continue;
            }
        }
    }
}
