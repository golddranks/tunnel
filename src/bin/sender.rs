const DEFAULT_BIND_PORT: &str = ":10443";

fn main() {
    let mut args = std::env::args();

    args.next(); // Throw away the first arg, it's just the name of the binary

    let psk = if let Some(a) = args.next() {
        a
    } else {
        eprintln!("You should provide the pre-shared key as the first argument!");
        std::process::exit(2);
    };

    let addr = if let Some(mut a) = args.next() {
        if !a.contains(':') {
            a.push_str(DEFAULT_BIND_PORT);
        }
        a
    } else {
        eprintln!("You should provide the receiver address and port as the second argument!");
        std::process::exit(1);
    };

    let identity = if let Some(i) = args.next() {
        i
    } else {
        "tunnel simple sender".to_owned()
    };

    let mut stream = match tunnel::connect_simple(&addr, &identity, psk.as_bytes()) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Error when connecting! {}", e);
            std::process::exit(5);
        }
    };

    eprintln!("Connected to {}", addr);

    let stdin = std::io::stdin();
    let mut handle = stdin.lock();
    match std::io::copy(&mut handle, &mut stream) {
        Ok(bytes) => {
            eprintln!("{} bytes sent.", bytes);
        }
        Err(e) => {
            eprintln!("Error while sending! {}", e);
            std::process::exit(6);
        }
    }
    if let Err(e) = stream.shutdown() {
        eprintln!("Error while shutting down the stream! {}", e);
        std::process::exit(7);
    }
}
