use openssl::ssl::SslStream;
use std::io::{Read, Result as IoResult, Write};

use crate::error::TunnelError;

// This is an abstraction around a writeable and readable TLS stream.
// This is to avoid revealing implementation details that might break things
// if we change or update our TLS backend. Besides the `Read` and `Write`
// traits, this type implements a `shutdown` method that safely shuts the
// TLS connection down. Note that the shutdown can fail, so you should always
// call the method and check the result.
#[derive(Debug)]
pub struct TunnelStream<S> {
    pub(crate) tls_stream: SslStream<S>,
}

impl<S> TunnelStream<S>
where
    S: Read + Write,
{
    pub fn shutdown(&mut self) -> Result<(), TunnelError> {
        self.tls_stream
            .shutdown()
            .map_err(|e| TunnelError::from(e, "Error on TLS shutdown."))?;
        Ok(())
    }
}

impl<S> Read for TunnelStream<S>
where
    SslStream<S>: Read,
{
    fn read(&mut self, buf: &mut [u8]) -> IoResult<usize> {
        self.tls_stream.read(buf)
    }
}
impl<S> Write for TunnelStream<S>
where
    SslStream<S>: Write,
{
    fn write(&mut self, buf: &[u8]) -> IoResult<usize> {
        self.tls_stream.write(buf)
    }

    fn flush(&mut self) -> IoResult<()> {
        self.tls_stream.flush()
    }
}
