use std::error::Error;
use std::fmt::{self, Display};

#[derive(Debug)]
pub struct TunnelError {
    ctx: &'static str,
    inner: Option<Box<dyn Error + 'static>>,
}

impl TunnelError {
    pub fn from(e: impl Error + 'static, ctx: &'static str) -> TunnelError {
        Self {
            ctx,
            inner: Some(Box::new(e)),
        }
    }

    pub fn new(ctx: &'static str) -> TunnelError {
        Self {
            ctx,
            inner: None,
        }
    }
}

impl Error for TunnelError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.inner.as_ref().map(AsRef::as_ref)
    }
}

impl Display for TunnelError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.inner {
            Some(e) => write!(f, "{}: {}", self.ctx, e),
            None => write!(f, "{}", self.ctx),
        }
    }
}
