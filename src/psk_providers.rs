use std::sync::Arc;

pub trait PskProvider {
    fn get_psk(&self, identity: &str) -> Result<&[u8], ()>;
}

#[derive(Clone, Debug)]
pub struct SimplePskProvider {
    psk: Arc<Vec<u8>>,
}

impl SimplePskProvider {
    pub fn new(psk: impl Into<Vec<u8>>) -> Self {
        Self {
            psk: Arc::new(psk.into()),
        }
    }
}

impl PskProvider for SimplePskProvider {
    fn get_psk(&self, _identity: &str) -> Result<&[u8], ()> {
        Ok(self.psk.as_slice())
    }
}
